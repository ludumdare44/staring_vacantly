extends CanvasLayer

onready var bar = $container/bar
onready var player = get_owner().get_node("player")

var animated_health = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	bar.max_value = player.MAX_THICCNESS
	update_health(player.thiccness)

func update_health(new_value):
	bar.value = new_value

