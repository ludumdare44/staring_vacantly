extends Sprite

export (Resource) var next_scene


onready var player = get_node("/root/world/player")
onready var state = get_node("/root/currentScene")

func _ready():
	pass # Replace with function body.

#func _process(delta):
#	pass

func _on_WarpArea_body_entered(body):
	if body.get_groups().has("player"):
		AudioManager.play_sound_effect("scenechange")
		#get_tree().change_scene("res://scenes/level2.tscn")
		state.score += player.thiccness
		get_tree().change_scene(next_scene.resource_path)
