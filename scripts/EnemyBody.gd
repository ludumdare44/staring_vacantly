extends KinematicBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const GRAVITY = 20
const FLOOR_NORMAL = Vector2(0, -1)
var motion = Vector2()
const DX = 100

const MAX_HEALTH = 100
const MIN_HEALTH = 0
var health = MAX_HEALTH
var damage = 50

var kill_y: int = 1000

var damaging_player = false

onready var player = get_node("/root/world/player")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if position.y > kill_y:
		get_parent().queue_free()
		return

	if health <= MIN_HEALTH:
		get_parent().queue_free()
		return

	motion.y += GRAVITY
	follow_player()
	motion = move_and_slide(motion, FLOOR_NORMAL)
	#print(motion)


func follow_player():
	var ppos = player.position
	# Area2D (SpawnArea)
	# ...
	# |- Node (Enemy)
	#    |- KinematicBody2D (EnemyBody) *
	#       |- CollisionShape2D
	var area: Area2D = get_parent().get_parent()
	var colshape: CollisionShape2D = get_child(2).get_child(0)
	ppos.x -= colshape.shape.extents.x
	#ppos.x -= area.position.x + colshape.position.x
	ppos.x -= area.position.x
	ppos.x -= colshape.position.x

	var mpos = get_position()
	if ppos.x < mpos.x:
		motion.x = -DX
		$Sprite.flip_h = true
	else:
		motion.x = DX
		$Sprite.flip_h = false

func take_hit():
	print("Take hit")
	health -= damage
	$TextureProgress.updateHealth(health)

func _on_EnemyArea_body_entered(body):
	print("_on_EnemyArea_body_entered")
	if body == player:
		player.take_hit()
		damaging_player = true

func _on_EnemyArea_body_exited(body):
	if body == player:
		damaging_player = false

