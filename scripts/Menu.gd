extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
    AudioManager.play_low_music()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_PlayButton_focus_entered():
	AudioManager.play_sound_effect("uihover")
    
func _on_PlayButton_button_down():
	play()


func play():
    get_tree().change_scene(get_node("/root/currentScene").current_scene)
    AudioManager.play_sound_effect("uiclick")
    AudioManager.play_high_music()

func _process(delta):
	if Input.is_action_pressed("ui_accept"):
		get_node("/root/Control").play()

	if Input.is_action_pressed("ui_cancel"):
		get_tree().quit()


func _on_PlayButton_mouse_entered():
	AudioManager.play_sound_effect("uihover")
