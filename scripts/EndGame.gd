extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var state = get_node("/root/currentScene")

# Called when the node enters the scene tree for the first time.
func _ready():
	get_node("/root/Control/CanvasModulate/TextureRect/Label").text = String(state.score)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
